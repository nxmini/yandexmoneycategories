//
//  NetworkResponseValidator.swift
//  Categories
//
//  Created by p.baranov on 08.08.2018.
//  Copyright © 2018 p.baranov. All rights reserved.
//

import Foundation

class NetworkResponseValidator {
    enum NetworkResponseValidatorError: Error {
        case unsuccessfulResponseCode
    }
    
    func validateResponse(response: NetworkService.HTTPResponse) throws {
        if !(200..<300 ~= response.response.statusCode) {
            throw NetworkResponseValidator.NetworkResponseValidatorError.unsuccessfulResponseCode
        }
    }
}
