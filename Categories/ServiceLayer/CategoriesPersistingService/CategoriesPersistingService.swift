//
//  CategoriesPersistingService.swift
//  Categories
//
//  Created by p.baranov on 08.08.2018.
//  Copyright © 2018 p.baranov. All rights reserved.
//

import Foundation
import CoreData

struct CategoriesPersistingService {
    
    struct Constants {
        static let categoryEntityName = "CategoryMO"
    }
    
    enum ServiceError: Error {
        case unableToCreateRootCategory
    }
    
    func saveCategories(_ categories:[Category],
                        inManagedObjectContext context: NSManagedObjectContext) throws {
        
        //simpliest way to update cache is to remove all instances and then write new data
        let fetchRequest = NSFetchRequest<CategoryMO>(entityName: Constants.categoryEntityName)
        fetchRequest.includesPropertyValues = false
        let fetchedCategories = try context.fetch(fetchRequest)
        for category in fetchedCategories {
            context.delete(category)
        }
        
        var rootCategory = try self.createRootCategory(in: context)
        
        @discardableResult func recursiveSave(root: Category) -> CategoryMO? {
            let categoryMO = NSEntityDescription.insertNewObject(forEntityName: Constants.categoryEntityName, into: context) as? CategoryMO
            if let categoryMO = categoryMO {
                categoryMO.title = root.title
                if let id = root.id {
                    categoryMO.id = Int32(id)
                }
                var children = [CategoryMO]()
                for child in root.subs {
                    if let childMO = recursiveSave(root: child) {
                        children.append(childMO)
                    }
                }
                categoryMO.subs = NSOrderedSet(array: children)
                return categoryMO
            }
            return nil
        }
        
        var subs = [CategoryMO]()
        for category in categories {
            if let sub = recursiveSave(root: category) {
                subs.append(sub)
            }
        }
        rootCategory.subs = NSOrderedSet(array: subs)
    }
    
    func fetchCategories(from context: NSManagedObjectContext) throws -> [Category] {
        
        guard let rootCategory = try self.fetchRootCategory(from: context) else {
            return []
        }
        
        func recursiveTransform(root: CategoryMO) -> Category {
            var children = [Category]()
            if let subs = root.subs {
                for sub in subs {
                    if let subCategory = sub as? CategoryMO {
                        let category = recursiveTransform(root: subCategory)
                        children.append(category)
                    }
                }
            }
            return Category(id: Int(root.id), title: root.title ?? "", subs: children)
        }
        
        let categories = recursiveTransform(root: rootCategory)
        return categories.subs
    }
    
    private func fetchRootCategory(from context: NSManagedObjectContext) throws -> CategoryMO? {
        let predicate = NSPredicate.init(format: "root == true")
        let fetchRequest = NSFetchRequest<CategoryMO>(entityName: Constants.categoryEntityName)
        fetchRequest.predicate = predicate
        let rootCategories = try context.fetch(fetchRequest)
        return rootCategories.first
    }
    
    private func createRootCategory(in context: NSManagedObjectContext) throws -> CategoryMO {
        guard let categoryMO = NSEntityDescription.insertNewObject(forEntityName: Constants.categoryEntityName, into: context) as? CategoryMO else {
            throw ServiceError.unableToCreateRootCategory
        }
        categoryMO.root = true
        
        return categoryMO
    }
}
