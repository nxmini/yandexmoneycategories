//
//  CoreDataService.swift
//  Categories
//
//  Created by p.baranov on 08.08.2018.
//  Copyright © 2018 p.baranov. All rights reserved.
//

import Foundation
import CoreData

class CoreDataService {
    
    private let backgroundContext: NSManagedObjectContext
    private let backgroundContextAccessQueue = DispatchQueue(label: "CoreDataServiceContextAccessQueue", qos: .background)
    
    init?(modelName: String) {
        guard let modelURL = Bundle.main.url(forResource: modelName, withExtension: "momd"),
        let managedObjectModel = NSManagedObjectModel(contentsOf: modelURL) else {
            return nil
        }
        
        
        let persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        
        let fileManager = FileManager.default
        let storeName = "\(modelName).sqlite"
        let documentsDirectoryURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let persistentStoreURL = documentsDirectoryURL.appendingPathComponent(storeName)
        
        do {
            try persistentStoreCoordinator.addPersistentStore(ofType: NSSQLiteStoreType,
                                                              configurationName: nil,
                                                              at: persistentStoreURL,
                                                              options: nil)
            let managedObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
            managedObjectContext.persistentStoreCoordinator = persistentStoreCoordinator
            self.backgroundContext = managedObjectContext
        } catch {
            return nil
        }
    }
    
    func executeAndSave(job: @escaping (NSManagedObjectContext) throws -> Void, completion: ((Result<Void>)->Void)? = nil) {
        let context = self.backgroundContext

        self.backgroundContextAccessQueue.async {
            var jobError: Error? = nil
            context.performAndWait {
                do {
                    try job(context)
                } catch {
                    jobError = error
                }
            }
            
            do {
                if let jobError = jobError {
                    throw jobError
                }
                try context.save()
                DispatchQueue.main.async {
                    completion?(.success(()))
                }
            } catch {
                context.rollback()
                DispatchQueue.main.async {
                    completion?(.failure(error))
                }
            }
        }
    }
    
    func execute<T>(job: @escaping (NSManagedObjectContext) throws -> T, completion: ((Result<T>)->Void)? = nil) {
        let context = self.backgroundContext
        context.perform {
            do {
                let result = try job(context)
                DispatchQueue.main.async {
                    completion?(.success(result))
                }
            } catch {
                DispatchQueue.main.async {
                    completion?(.failure(error))
                }
            }
        }
    }
}
