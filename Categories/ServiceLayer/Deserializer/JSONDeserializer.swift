//
//  JSONDeserializer.swift
//  Categories
//
//  Created by p.baranov on 08.08.2018.
//  Copyright © 2018 p.baranov. All rights reserved.
//

import Foundation

struct JSONDeserializer {
    func deserialize<T>(data: Data, as: T.Type) throws -> T where T: Decodable {
        let decoder = JSONDecoder()
        let result = try decoder.decode(T.self, from: data)
        return result
    }
}
