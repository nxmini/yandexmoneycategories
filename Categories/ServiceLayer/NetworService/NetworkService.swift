//
//  NetworkService.swift
//  Categories
//
//  Created by p.baranov on 07.08.2018.
//  Copyright © 2018 p.baranov. All rights reserved.
//

import Foundation



struct NetworkService {
    
    enum NetworkServiceError: Error {
        case wrongResponse
    }
    
    struct HTTPResponse {
        let body: Data
        let response: HTTPURLResponse
    }
    
    private let urlSession = URLSession(configuration: URLSessionConfiguration.default)
    
    func request(url: URL, httpMethod: String = "GET", completion: @escaping (Result<HTTPResponse>)->Void) {
        //URL кэши помешают проверить работу локальной базы данных, поэтому устанавливаю политику игнорирования кеша
        var request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 60)
        request.httpMethod = httpMethod
        
        let task = self.urlSession.dataTask(with: request) { data, response, error in
            if let error = error {
                completion(.failure(error))
            } else {
                do {
                    let httpResponse = try self.createHttpResponse(data: data, response: response)
                    completion(.success(httpResponse))
                } catch {
                    completion(.failure(error))
                }
            }
        }
        task.resume()
    }
    
    private func createHttpResponse(data: Data?, response: URLResponse?) throws -> HTTPResponse {
        guard let httpURLResponse = response as? HTTPURLResponse else {
            throw NetworkServiceError.wrongResponse
        }
        return HTTPResponse(body: data ?? Data(), response: httpURLResponse)
    }
}
