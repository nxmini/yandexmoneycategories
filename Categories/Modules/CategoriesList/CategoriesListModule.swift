//
//  CategoriesListModule.swift
//  Categories
//
//  Created by p.baranov on 08.08.2018.
//  Copyright © 2018 p.baranov. All rights reserved.
//

import Foundation

// VIEW

struct CategoryDisplayData {
    let name: String
    let level: Int
}

protocol CategoriesListView: class {
    func hideList()
    func showList()
    func startProgressIndicator()
    func stopProgressIndicator()
    func updateCategoriesList(_ displayData: [CategoryDisplayData])
    func presentError(_ error: Error)
}

protocol CategoriesListViewOutput: class {
    func showCategories()
    func reloadCategories()
}

// INTERACTOR

protocol CategoriesListInteractorInput {
    func fetchCachedCategories()
    func loadCategoriesFromServer()
}

protocol CategoriesListInteractorOutput: class {
    func didFailToFetchCachedCategories(error: Error)
    func didFetchCachedCategories(categories: [Category])
    
    func didFailToLoadCategoriesFromServer(error: Error)
    func didLoadCategoriesFromServer(categories: [Category])
}
