//
//  CategoriesListModuleFactory.swift
//  Categories
//
//  Created by p.baranov on 08.08.2018.
//  Copyright © 2018 p.baranov. All rights reserved.
//

import Foundation
import UIKit

struct CategoriesListModuleFactory {
    func buildModule() -> UIViewController {
        let networkService = NetworkService()
        let responseValidator = NetworkResponseValidator()
        let deserializer = JSONDeserializer()
        let coreDataService = CoreDataService(modelName: "Categories")
        let categoriesPersistingService = CategoriesPersistingService()
        
        let interactor = CategoriesListInteractor(networkService: networkService,
                                                  responseValidator:responseValidator,
                                                  deserializer: deserializer,
                                                  categoriesPersistingService: categoriesPersistingService)
        interactor.coreDataService = coreDataService
        
        let view = CategoriesListViewController()
        let presenter = CategoriesListPresenter(interactor: interactor, view: view)
        view.presenter = presenter
        interactor.output = presenter
        return view
    }
}
