//
//  CategoriesListPresenter.swift
//  Categories
//
//  Created by p.baranov on 08.08.2018.
//  Copyright © 2018 p.baranov. All rights reserved.
//

import UIKit

class CategoriesListPresenter {
    
    enum State {
        case initial
        case loading
        case loaded
        case error
    }

    var interactor: CategoriesListInteractorInput
    weak var view: CategoriesListView?
    var state: State = .initial
    var currentDisplayData:[CategoryDisplayData] = []
    
    init(interactor: CategoriesListInteractorInput, view: CategoriesListView) {
        self.interactor = interactor
        self.view = view
    }
    
    func prepareDisplayData(categories: [Category]) -> [CategoryDisplayData] {
        var result = [CategoryDisplayData]()
        for category in categories {
            DFS(root: category) { (category, level) in
                let displayData = CategoryDisplayData(name: category.title, level: level)
                result.append(displayData)
            }
        }
        return result
    }
    
    func finishLoading(categories: [Category]) {
        let displayData = self.prepareDisplayData(categories: categories)
        self.currentDisplayData = displayData
        self.state = .loaded
        self.view?.updateCategoriesList(displayData)
        self.view?.showList()
        self.view?.stopProgressIndicator()
    }
}


extension CategoriesListPresenter: CategoriesListViewOutput {
    
    func showCategories() {
        switch state {
        case .initial:
            self.view?.hideList()
            self.view?.startProgressIndicator()
            self.state = .loading
            self.interactor.fetchCachedCategories()
        case .loaded, .error:
            self.view?.updateCategoriesList(self.currentDisplayData)
        default:
            break
        }
    }
    
    func reloadCategories() {
        self.state = .loading
        self.interactor.loadCategoriesFromServer()
    }
}

extension CategoriesListPresenter: CategoriesListInteractorOutput {
    func didFailToFetchCachedCategories(error: Error) {
        //ignoring errors of local cache, trying to fetch from network
        self.interactor.loadCategoriesFromServer()
    }
    
    func didFetchCachedCategories(categories: [Category]) {
        if !categories.isEmpty {
            self.finishLoading(categories: categories)
        } else {
            self.interactor.loadCategoriesFromServer()
        }
    }
    
    func didFailToLoadCategoriesFromServer(error: Error) {
        self.state = .error
        self.view?.showList()
        self.view?.stopProgressIndicator()
        self.view?.presentError(error)
    }
    
    func didLoadCategoriesFromServer(categories: [Category]) {
        self.finishLoading(categories: categories)
    }
}
