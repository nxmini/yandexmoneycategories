//
//  CategoriesListInteractor.swift
//  Categories
//
//  Created by p.baranov on 08.08.2018.
//  Copyright © 2018 p.baranov. All rights reserved.
//

import Foundation

class CategoriesListInteractor: CategoriesListInteractorInput {
    
    enum InteractorError: Error {
        case CoreDataServiceNotInitialized
    }
    
    struct Constants {
        static let fetchURL = "https://money.yandex.ru/api/categories-list"
    }
    
    weak var output: CategoriesListInteractorOutput?
    let networkService: NetworkService
    let responseValidator: NetworkResponseValidator
    let deserializer: JSONDeserializer
    var categoriesPersistingService: CategoriesPersistingService
    var coreDataService: CoreDataService?
    
    init(networkService: NetworkService,
         responseValidator: NetworkResponseValidator,
         deserializer: JSONDeserializer,
         categoriesPersistingService: CategoriesPersistingService) {
        self.networkService = networkService
        self.responseValidator = responseValidator
        self.deserializer = deserializer
        self.categoriesPersistingService = categoriesPersistingService
    }
    
    func fetchCachedCategories() {
        guard let coreDataService = self.coreDataService else {
            self.output?.didFailToFetchCachedCategories(error: InteractorError.CoreDataServiceNotInitialized)
            return
        }
        
        let service = self.categoriesPersistingService
        coreDataService.execute(job: { (context) -> [Category] in
            return try service.fetchCategories(from: context)
        }, completion: { [weak self] result in
            guard let output = self?.output else {
                return
            }
            switch result {
            case .success(let categories):
                output.didFetchCachedCategories(categories: categories)
            case .failure(let error):
                output.didFailToFetchCachedCategories(error: error)
            }
        })
    }
    
    func loadCategoriesFromServer() {
        guard let url = URL(string: Constants.fetchURL) else {
            self.output?.didFailToLoadCategoriesFromServer(error: NSError(domain: "CategoriesListInteractor", code: -1, userInfo: [:]))
            return
        }
        self.networkService.request(url: url) { [weak self] result in
            guard let strongSelf = self else {
                return
            }
            
            do {
                let response = try result.dematerialize()
                try strongSelf.responseValidator.validateResponse(response: response)
                let categories = try strongSelf.deserializer.deserialize(data: response.body, as: [Category].self)
                DispatchQueue.main.async {
                    strongSelf.output?.didLoadCategoriesFromServer(categories: categories)
                    strongSelf.persistCategories(categories)
                }
            } catch {
                DispatchQueue.main.async {
                    strongSelf.output?.didFailToLoadCategoriesFromServer(error: error)
                }
            }
        }
    }
    
    private func persistCategories(_ categories: [Category]) {
        guard let coreDataService = self.coreDataService else {
            return
        }
        
        coreDataService.executeAndSave(job: { (context) in
            try self.categoriesPersistingService.saveCategories(categories, inManagedObjectContext: context)
        })
    }
    
}
