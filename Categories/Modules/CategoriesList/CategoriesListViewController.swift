//
//  ViewController.swift
//  Categories
//
//  Created by p.baranov on 07.08.2018.
//  Copyright © 2018 p.baranov. All rights reserved.
//

import UIKit

class CategoriesListViewController: UIViewController {
    
    struct Constants {
        static let categoryCellReuseID = "CategoryCellReuseID"
    }
    
    var presenter: CategoriesListViewOutput?
    
    weak var tableView: UITableView?
    weak var refreshControl: UIRefreshControl?
    weak var activityIndicator: UIActivityIndicatorView?
    
    var displayData = [CategoryDisplayData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        // Do any additional setup after loading the view, typically from a nib.
        
        let tableView = UITableView(frame: self.view.bounds, style: .plain)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: Constants.categoryCellReuseID)
        self.view.addSubview(tableView)
        self.tableView = tableView
        
        let hConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[tableView]-0-|",
                                                          options: [],
                                                          metrics: [:],
                                                          views: ["tableView": tableView])
        let vConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[tableView]-0-|",
                                                          options: [],
                                                          metrics: [:],
                                                          views: ["tableView": tableView])
        
        self.view.addConstraints(hConstraints)
        self.view.addConstraints(vConstraints)
        
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        self.view.addSubview(activityIndicator)
        self.activityIndicator = activityIndicator
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        let activityIndicatorCenterXConstraint = NSLayoutConstraint(item: activityIndicator,
                                                                    attribute: .centerX,
                                                                    relatedBy: .equal,
                                                                    toItem: self.view,
                                                                    attribute: .centerX,
                                                                    multiplier: 1.0,
                                                                    constant: 0.0)
        let activityIndicatorCenterYConstraint = NSLayoutConstraint(item: activityIndicator,
                                                                    attribute: .centerY,
                                                                    relatedBy: .equal,
                                                                    toItem: self.view,
                                                                    attribute: .centerY,
                                                                    multiplier: 1.0,
                                                                    constant: 0.0)
        self.view.addConstraints([activityIndicatorCenterXConstraint, activityIndicatorCenterYConstraint])
        self.view.setNeedsLayout()
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(CategoriesListViewController.onPullToRefresh), for: .valueChanged)
        tableView.addSubview(refreshControl)
        self.refreshControl = refreshControl
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.presenter?.showCategories()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func onPullToRefresh(sender: UIRefreshControl) {
        self.presenter?.reloadCategories()
    }
}

extension CategoriesListViewController: CategoriesListView {
    func hideList() {
        self.tableView?.isHidden = true
    }
    
    func showList() {
        self.tableView?.isHidden = false
    }
    
    func presentError(_ error: Error) {
        let alert = UIAlertController(title: "Ошибка", message: "Что-то пошло не так", preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(alertAction)
        self.present(alert, animated: true, completion: nil)
        print(error)
    }
    
    
    func startProgressIndicator() {
        self.activityIndicator?.startAnimating()
    }
    
    func stopProgressIndicator() {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3) {
            self.refreshControl?.endRefreshing()
        }
        self.activityIndicator?.stopAnimating()
    }

    func updateCategoriesList(_ displayData: [CategoryDisplayData]) {
        self.displayData = displayData
        self.tableView?.reloadData()
    }
}

extension CategoriesListViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.displayData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.categoryCellReuseID) ?? UITableViewCell()
        let displayData = self.displayData[indexPath.row]
        cell.textLabel?.text = displayData.name
        cell.indentationLevel = displayData.level
        cell.backgroundColor = UIColor.white
        return cell
    }
}

extension CategoriesListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

