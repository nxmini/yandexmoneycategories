//
//  Category+Decodable.swift
//  Categories
//
//  Created by p.baranov on 07.08.2018.
//  Copyright © 2018 p.baranov. All rights reserved.
//

import Foundation

extension Category: Decodable {
    enum Keys: CodingKey {
        case id
        case title
        case subs
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: Keys.self)
        self.id = try container.decodeIfPresent(Int.self, forKey: .id)
        self.title = try container.decode(String.self, forKey: .title)
        self.subs = try container.decodeIfPresent([Category].self, forKey: .subs) ?? []
    }
}
