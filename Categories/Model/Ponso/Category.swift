//
//  Category.swift
//  Categories
//
//  Created by p.baranov on 07.08.2018.
//  Copyright © 2018 p.baranov. All rights reserved.
//

import Foundation

struct Category {
    let id: Int?
    let title: String
    let subs: [Category]
}
