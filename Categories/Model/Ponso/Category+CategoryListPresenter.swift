//
//  Category+CategoryListPresenter.swift
//  Categories
//
//  Created by p.baranov on 10.08.2018.
//  Copyright © 2018 p.baranov. All rights reserved.
//

import Foundation

protocol TreeNode {
    func children() -> [Self]
}

func DFS<T>(root: T, level: Int = 0, visitFunction: @escaping (T, Int)->Void) where T: TreeNode {
    visitFunction(root, level)
    for child in root.children() {
        DFS(root: child, level: level+1, visitFunction: visitFunction)
    }
}

extension Category: TreeNode {
    func children() -> [Category] {
        return self.subs
    }
}
