
//
//  Result.swift
//  Categories
//
//  Created by p.baranov on 07.08.2018.
//  Copyright © 2018 p.baranov. All rights reserved.
//

import Foundation

enum Result<T> {
    case success(T)
    case failure(Error)
    
    func dematerialize() throws -> T {
        switch self {
        case .success(let value):
            return value
        case .failure(let error):
            throw error
        }
    }
}
